export default class Loader {
	constructor(onAssetsLoaded) {
		this.loader = PIXI.loader;
		this.loadAssets();
		this.loader.once('complete', onAssetsLoaded);
		this.loader.load();
	}
	
	loadAssets() {
		this.loader.add('BG', require("../assets/bg_final.png"));
		this.loader.add('btn_active', require("../assets/btn_active.png"));
		this.loader.add('btn_inactive', require("../assets/btn_inactive.png"));
		this.loader.add('gem', require("../assets/gem.png"));
		this.loader.add('heart', require("../assets/heart.png"));
		this.loader.add('jackpot', require("../assets/jackpot.png"));
		this.loader.add('orange', require("../assets/orange.png"));
		this.loader.add('seven', require("../assets/seven.png"));
		this.loader.add('cherry', require("../assets/cherry.png"));	
		this.loader.add("leftArrow", require("../assets/Leftarrow.png"));
    	this.loader.add("rightArrow", require("../assets/right_arrow.png"));	
	}
}