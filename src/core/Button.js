export default class Button {
	constructor(onSpin) {
		this.onSpin = onSpin;
		this.button = null;
		this.leftSymbol;
		this.rightSymbol;
		this.position = {
			x: 750,
			y: 820
		};
		this.balanceVal = 500;
		this.stake = 1;

		this.addStake = function () {
            
            if (this.stake >= 1 && this.stake <= 5) {
                this.stake ++;
            }
		};
		
		this.minusStake = function () {
            
            if (this.stake > 1) {
                this.stake --;
            }
        };

		this.reduceBalance = function (){
            
            this.balanceVal = this.balanceVal - this.stake;
        }
		

		this.textStyle = {
			fontFamily: 'Arial',
			fontWeight: 'bold',
			fontSize: '76px',
			"fill": [
				"#7979c4",
				"#e1e1e1"
			],
			stroke: '#4a1850',
			strokeThickness: 5,
			wordWrap: true,
			wordWrapWidth: 440
		};

		this.container = new PIXI.Container();
		this.active = true;
		this.initialize();
		this.bindClick();
		this.onLeftbindClick();
		this.onRightbindClick();
		this.addBalance(this.balanceVal);
		this.addStakeText(this.stake);
		
		
	}
	
	initialize() {

		this.leftArrow = PIXI.loader.resources['leftArrow'].texture;
		this.rightArrow = PIXI.loader.resources['rightArrow'].texture;
		this.activeBtn = PIXI.loader.resources['btn_active'].texture;
		this.inactiveBtn = PIXI.loader.resources['btn_inactive'].texture;
		this.container.position.x = this.position.x;
		this.container.position.y = this.position.y;
		this.button = this.createButton();
		this.leftSymbol = this.createLeftArrow();
		this.rightSymbol = this.createRightArrow();
		this.container.addChild(this.button, this.leftSymbol, this.rightSymbol);
		
	}
	

	createButton() {
		let btn = new PIXI.Sprite(this.activeBtn);
		btn.buttonMode = true;
		btn.interactive = true;
		btn.anchor.set(0.5);
		return btn;
	}

	createLeftArrow() {
		let lftArrow = new PIXI.Sprite(this.leftArrow);
		lftArrow.buttonMode = true;
		lftArrow.interactive = true;
		lftArrow.anchor.set(0.5)
		lftArrow.position.x = 200;
		lftArrow.width = 50;
		lftArrow.height = 50;
		
		
		return lftArrow;
	}

	

	createRightArrow() {
		let rghtArrow = new PIXI.Sprite(this.rightArrow);
		rghtArrow.buttonMode = true;
		rghtArrow.interactive = true;
		rghtArrow.anchor.set(0.5)
		rghtArrow.position.x = 400;
		rghtArrow.width = 50;
		rghtArrow.height = 50;
		return rghtArrow;
		
	}

	onLeftbindClick() {
		this.leftSymbol.on('click', this.onLeftArrowClick.bind(this))
	}

	onLeftArrowClick() {

		this.minusStake();
		this.stakeText.text = this.stake;
	}

	
	onRightbindClick() {
		this.rightSymbol.on('click', this.onRightArrowClick.bind(this));
	}



	onRightArrowClick() {
		
		this.addStake();
		this.stakeText.text = this.stake;
		
	}
	



	addBalance(balance) {
		this.balance = new PIXI.Text(balance, this.textStyle);
		this.balance.position.x = -400;
		this.balance.position.y = -50;
		this.container.addChild(this.balance);

	}

	addStakeText(stakeText) {
		this.stakeText = new PIXI.Text(stakeText, this.textStyle);
		this.stakeText.position.x = 280;
		this.stakeText.position.y = -50;
		this.container.addChild(this.stakeText);
	}

	
	bindClick() {
		this.button.on('click', this.onButtonClick.bind(this));
	}
	
	onButtonClick() {
		if (!this.active) return;
		this.reduceBalance();
		this.balance.text = this.balanceVal;
		this.buttonActiveToggle();
		this.onSpin();
	}

	buttonActiveToggle() {
		this.active = !this.active;
		this.button.texture = this.active ? this.activeBtn : this.inactiveBtn;
	}
}